module.exports = async function(fastify, options) {
	fastify.get('/', function(req, reply) {
			reply.send({
				hello: 'Baby'
			})
		})
		.get('/test', function(req, reply) {
			reply.send({
				hello: 'test'
			})
		})
		.get('/test/:id', async function(req, reply) {
			req.log.info('test,id=' + req.params.id);
			reply.send({
				hello: 'test,id=' + req.params.id
			})

		})
		.get('/mg', async function(req, reply) {
			const mg = require('../mongo').mg;
			let res = await mg();	
			console.log(res)
			reply.send(res) ;
		})
}