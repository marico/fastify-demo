/**
 * New node file
 */
const fastify = require('fastify')({
	logger: true
});

//1.1 start

/*fastify.get('/', async function(request, reply) {
	return {test:'test000'}
})
fastify.listen(3000, function(err) {
	if(err) throw err
	console.log(`server listening on ${fastify.server.address().port}`)
})*/

// 1.2 schema

/*
const opt = {
	schema: {
		response: {
			200: {
				type: "object",
				properties: {
					hello: {
						type: "string"
					},
					test: {
						type:  "string"
					}
				}
			}
		}
	}
}

//声明一个包含schema的路由
fastify.get('/', opt, function(request, reply) {
	reply.send({
		test: 'test',
		hello: 'world'
	})
})
*/

//1.3 Register
/*
fastify.register(require('./route/route.js'));
const opt = {
	hello:'world',
	test:true
}
*/

//2.1 fastify.server   node的server对象

//2.2 ready
/*
fastify.ready(err=>{
	if(err) throw err;
	console.log('fastify is ready')

})
*/
/*fastify.ready().then(() => {
	console.log('ready')
})*/

//2.3 listen  所有插件加载完毕 在指定端口启动服务器

fastify.register(require('./route/route.js'));

/*fastify.listen(3000, function(err) {
	if(err) throw err
	console.log(`server listening on ${fastify.server.address().port}`)
})*/
/*
fastify.listen(3000,'127.0.0.1', function(err) {
	if(err) throw err
	console.log(`server listening on ${fastify.server.address().address}:${fastify.server.address().port}`)
})
*/

//2.4 route

//2.5 routes iterator

fastify.ready().then(() => {
	/*for (let route of fastify) {
		console.log(route)
	}*/
})

//2.6 close  fastify.close(callback)

//2.7 decorate*  装饰器

// 3.  routes
//3.1  Full declaration
//fastify.route(options) { method,url,schema{body,querystring,properties ,params,response},beforeHandler(request, reply, done),handler(request, reply),schemaCompiler(schema)},

//3.2 Shorthand declaration
/*
	 fastify.get(path, [options], handler)
	fastify.head(path, [options], handler)
	fastify.post(path, [options], handler)
	fastify.put(path, [options], handler)
	fastify.delete(path, [options], handler)
	fastify.options(path, [options], handler)
	fastify.patch(path, [options], handler)
	 * */
//3.3
fastify.listen(3000).then(() => {
	console.log(`server ${fastify.server.address().address}:${fastify.server.address().port} is running`)
}).catch(err => {
	console.log(err)
})