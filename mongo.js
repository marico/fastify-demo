async function mg() {
	let opt = {
		mongoAddr: 'mongodb://linux.nongshiye.com:27017/fastify',
		mongoPort: 27017,
		cfg: {
			auto_reconnect: true
		}
	}

	var MongoClient = require('mongodb').MongoClient,
		assert = require('assert');

	// Connection URL

	// Use connect method to connect to the server
	MongoClient.connect(opt.mongoAddr, function(err, db) {
		assert.equal(null, err);
		console.log("Connected successfully to server");
		var collection = db.collection('test');
		// Insert some documents
		let res = collection.insertMany([{
			test1: 1
		}, {
			test2: 2
		}, {
			test4: 3
		}], async function(err, result) {
			assert.equal(err, null);
			assert.equal(3, result.result.n);
			assert.equal(3, result.ops.length);
			console.log("Inserted 3 documents into the collection");

		});
		db.close();
	});
	return {
		'mg': 'res'
	}

}
exports.mg = mg;